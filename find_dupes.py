from checksum import create_checksum
from diskwalk import diskwalk
from os.path import getsize
import sys
import os


def findDupes(path):
    record = {}
    dup = {}
    d = diskwalk(path)
    files = d.paths()
    for file in files:
        compound_key = (getsize(file), create_checksum(file))
        if compound_key in record:
            dup[file] = record[compound_key]
        else:
            record[compound_key] = file
    return dup


if __name__ == '__main__':
    for _file in findDupes(sys.argv[1]).items():
        print "file is duplicate from %s to %s:" % (_file[1], _file[0])
        os.system('echo "" > %s' % _file[0])
